package nl.utwente.di.temperatures;

public class Quoter {

    public double getTemperature(String temperature) {
        return Integer.parseInt(temperature)*1.8 + 32;
    }
}
